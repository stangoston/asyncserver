/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package asyncserver;

import asyncserver.business.ListeningMgr;

/**
 *
 * @author stan
 */
public class AsyncServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ListeningMgr l = new ListeningMgr();
        l.run();
    }
    
}
